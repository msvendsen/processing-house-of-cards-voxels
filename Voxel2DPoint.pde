public class Voxel2DPoint{
	
	public float calculatedScale;
	public float x;
	public float y;

	Voxel2DPoint(float calcScale){
		this.calculatedScale = calcScale;
	}

	public void setX(float value){
		this.x = value * this.calculatedScale;
	}

	public void setY(float value){
		this.y = value * this.calculatedScale;
	}

}