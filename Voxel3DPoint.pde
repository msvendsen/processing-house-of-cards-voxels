public class Voxel3DPoint{
	
	public float x;
	public float y;
	public float z;
	public int intensity;

	Voxel3DPoint(float xPos, float yPos, float zPos){
		this.x = xPos;
		this.y = yPos;
		this.z = zPos;
	}

	public Voxel2DPoint get2DPoint(float focalLength){
		float calculatedScale = focalLength / (this.z + focalLength);
		Voxel2DPoint vox2d = new Voxel2DPoint(calculatedScale);
		vox2d.setX(this.x);
		vox2d.setY(this.y);
		return vox2d;
	}

}