public class VoxelScene{
	
	private int width;
	private int height;
	private float focalLength;
	public float sceneRotationX = 0;
	public float sceneRotationY = 0;
	public float sceneScale = 1;
	private VoxelMatrix3D matrix3d;
	private Voxel3DPoint[] scenePoints3D;
	private Voxel3DPoint[] transformed3DPoints;
	public  Voxel2DPoint[] scenePoints2D;

	VoxelScene(float sceneFocalLength, int sceneWidth, int sceneHeight){
		this.width = sceneWidth;
		this.height = sceneHeight;
		this.focalLength = sceneFocalLength;
		matrix3d = new VoxelMatrix3D();
	}

	public void initWithFile(String filePath){
		String[] lines = loadStrings(filePath);
		scenePoints3D = new Voxel3DPoint[lines.length];
		scenePoints2D = new Voxel2DPoint[lines.length];
		for(int i=0; i<lines.length; i++){
			String[] lineData = split(lines[i], ",");
			Voxel3DPoint vp3D = new Voxel3DPoint(float(lineData[0]), float(lineData[1]), float(lineData[2]));
			vp3D.intensity = int(lineData[3]);
			scenePoints3D[i] = vp3D;
			scenePoints2D[i] = vp3D.get2DPoint(this.focalLength);
		}
	}

	public void initWithVoxelData(Voxel3DPoint[] points){
		scenePoints3D = points;
		scenePoints2D = new Voxel2DPoint[scenePoints3D.length];
		for(int i=0; i < scenePoints3D.length; i++){
			scenePoints2D[i] = scenePoints3D[i].get2DPoint(this.focalLength);
		}
	}



	public void render(){
		//translate(this.width/4, this.height/4);
		noStroke();
		this.matrix3d.identity();
		this.matrix3d.rotateX(this.sceneRotationX);
		this.matrix3d.rotateY(this.sceneRotationY);
		this.matrix3d.translate(width/4, height/4, 200);
		this.matrix3d.scaleMatrix(this.sceneScale, this.sceneScale, this.sceneScale);
		transformed3DPoints = this.matrix3d.transformPoints(scenePoints3D);

		for(int i=0; i<transformed3DPoints.length; i++){
			Voxel2DPoint vp2D = transformed3DPoints[i].get2DPoint(this.focalLength);
			fill(scenePoints3D[i].intensity);
			ellipse(vp2D.x, vp2D.y , 4 * vp2D.calculatedScale, 4 * vp2D.calculatedScale);
		}
		/*noStroke();
		for(int i=0; i<scenePoints2D.length; i++){
			if(scenePoints2D[i].calculatedScale > -1){
				fill(scenePoints3D[i].intensity);
				ellipse(scenePoints2D[i].x, scenePoints2D[i].y, 4 * scenePoints2D[i].calculatedScale, 4 * scenePoints2D[i].calculatedScale);
			}
		}*/
	}

}