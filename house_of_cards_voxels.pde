int width = 600;
int height = 600;
float focalLength = 1000;
float max_rotation;
Voxel3DPoint[][] pointData;

int totalFrames = 1000;
int currentFrame = 0;

VoxelScene scene;

void setup(){
	size(this.width, this.height);
	frameRate(24);

	max_rotation = PI * 0.3;

	loadAllData("HoC_AnimationData1_v1/", 1000);
	scene = new VoxelScene(this.focalLength, this.width, this.height);
	//scene.initWithFile("HoC_AnimationData1_v1/1.csv");

}

void draw(){
	background(0);
	frameRate(31);
	scene.initWithVoxelData(pointData[currentFrame]);
	scene.render();
	currentFrame += 1;
	if(currentFrame == totalFrames) currentFrame = 0;
	//saveFrame("output/frame-####.png");
}

void mouseMoved(){
	float rotationValueY = mouseX / float(this.width) * max_rotation * 2 - max_rotation;
	float rotationValueX = mouseY / float(this.height) * max_rotation * 2 - max_rotation;
	scene.sceneRotationY = rotationValueY;
	scene.sceneRotationX = rotationValueX;
}

void loadAllData(String baseDir, int fileCount){

	pointData = new Voxel3DPoint[fileCount][];
	for(int i=0; i<fileCount; i++){
		String file = baseDir;
		file += str(i+1);
		file += ".csv";
		println(file);
		String[] lines = loadStrings(file);
		pointData[i] = new Voxel3DPoint[lines.length];
		for(int j=0; j<lines.length; j++){
			String[] lineData = split(lines[j], ",");
			Voxel3DPoint vp3D = new Voxel3DPoint(float(lineData[0]), float(lineData[1]), float(lineData[2]));
			vp3D.intensity = int(lineData[3]);
			pointData[i][j] = vp3D;
		}
	}

}
